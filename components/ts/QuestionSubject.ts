import BaseOption from './BaseOption';
import GUID from './GUID';
import DataDefine, { QuestionDataTypes,QuestionTypes } from './QuestionDataTypes';
import { OrderByTypes } from './OrderByTypes';

export default class QuestionSubject<T extends BaseOption> {
   // 题目Id
    public Id: string = '';
    // 题目编号
    public CodeNum: string = '';
    public Title: string = '标题';
    public SelValue: string = '';
    public MustAnswer: boolean = true;
    public Options: T[] = [];
    public DataType: QuestionDataTypes = QuestionDataTypes.dtText;

    public OrderValue: OrderByTypes = OrderByTypes.obtVertical;
    public QuestionType:QuestionTypes=QuestionTypes.qtRadio;

    constructor() {
        this.Id = new GUID().toString();
    }
}
