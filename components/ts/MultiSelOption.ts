import BaseOption from './BaseOption';

export default class MultiSelOption extends BaseOption {
   public MultiValue: string = '';
   // 互斥题目
   public Exclusive: boolean = false;
}
